#!/bin/bash
# A script to clone all necessary repos when installing from scratch. the $HBP need to be set-up for this script to work successfully

ubuntu_version=`lsb_release -rs`
if [[ -z $HBP ]] ; then
  echo "Please set the \$HBP variable to your desired installation path first."
  exit
else
  if [[ -d $HBP ]] ; then
    echo "The NRP repos will be cloned to the already-existing directory" $HBP
  elif [[ -f $HBP ]] ; then
    echo "Invalid destination." $HBP "is a file."
    exit 1
  else
    echo "The NRP repos will be cloned to the newly created directory" $HBP
    mkdir -p -v $HBP
  fi
fi

# Get Bitbucket username to clone repos for developers
echo
if [ "$NRP_INSTALL_MODE" != "user" ]
then
  read -p "Enter your Bitbucket username: " user
fi
echo

function clone_repo {

  if [ "$#" -ne 1 ] ; then
    echo "Invalid number of arguments to clone_repo. Aborting."
    exit 1
  fi

  echo
  echo $1
  if [ -d "$1" ] ; then
    echo "Skipped, directory already exists."
  else
    if [ "$NRP_INSTALL_MODE" == "user" ]
    then
      git clone -b master --depth 1 https://bitbucket.org/hbpneurorobotics/$1.git $1
      pushd $1;git config remote.origin.fetch "+refs/heads/master*:refs/remotes/origin/master*";git pull
      if echo "$ubuntu_version" | grep 18 >> /dev/null; then
        git checkout master18
      fi
      popd
    else
      git clone $user@bitbucket.org:hbpneurorobotics/$1.git $1
    fi
  fi
  echo
}

# Cloning repos
option=$1
source $HBP/user-scripts/repos.txt

case $option in
backend)
  repos=(
    ${nrp_python[*]}
    GazeboRosPackages
    ${nrp_3rd_party[*]}
    gzweb
  )
  ;;
frontend)
  repos=(
    nrpBackendProxy
    ExDFrontend
    VirtualCoach
  )
  ;;
daint)
  repos=(
    ${nrp_python[*]}
    GazeboRosPackages
    ${nrp_3rd_party[*]}
    gzweb
    Models
    Experiments
  )
  ;;
*)
repos=(
  ${nrp_3rd_party[*]}
  ${nrp_repos[*]}
)
  ;;
esac

echo "Cloning repos:"
pushd $HBP > /dev/null
for repo in ${repos[@]} ; do clone_repo $repo ; done
popd > /dev/null
